import { useMutation } from 'react-query'
import { shareVideo } from '~/features/video/api/get-videos'
import {queryClient} from '~/lib/react-query'
import { ShareVideoRequest } from '~/features/video/types'
export const useShareVideo = () => {
  const shareVideoMutation = useMutation({
    mutationFn: async (data: ShareVideoRequest) => {
      const response = await shareVideo(data)
      return response
    },
    onSuccess: (result) => {
      queryClient.refetchQueries()
      console.log(result)
    }
  })

  return {
    shareYoutubeVideo: shareVideoMutation.mutateAsync
  }
}
