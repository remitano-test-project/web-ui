import { useQuery } from 'react-query'
import { getVideos } from '~/features/video/api/get-videos'
export const useGetDataVideo = (page: number) => {
  const { data, refetch } = useQuery<any, Error>({
    queryKey: ['videos', page],
    queryFn: () => getVideos({ page }),
    useErrorBoundary: false
  })

  return {
    data: data?.data?.data,
    pagination: data?.data?.pagination,
    refetchVideoData: refetch
  }
}
