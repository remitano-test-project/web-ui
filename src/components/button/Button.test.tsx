import { describe, test, expect, vi } from 'vitest'
import { render } from '@testing-library/react'
import { Button } from '~/components/button'

describe('Button component test.', () => {
  const buttonProps = {
    text: 'Button Test',
    className: 'priority:bg-red-300'
  }

  afterEach(() => {
    vi.restoreAllMocks()
  })

  test('Should show correct text', () => {
    const wrapper = render(<Button text={buttonProps.text} />)
    expect(wrapper).toBeTruthy()

    // Get button
    const button = wrapper.container.querySelector('button')
    expect(button?.textContent).toBe(buttonProps.text)
  })

  test('Should have override class', () => {
    const wrapper = render(<Button text={buttonProps.text} className={buttonProps.className} />)
    expect(wrapper).toBeTruthy()

    // Get button
    const button = wrapper.container.querySelector('button')
    expect(button?.className).contain(buttonProps.className)
  })

  test('Should call onClick function props when click', () => {
    const fnClick = vi.fn()
    const wrapper = render(<Button onClick={fnClick} text={buttonProps.text} className={buttonProps.className} />)
    expect(wrapper).toBeTruthy()

    wrapper.container.querySelector('button')?.click()
    expect(fnClick).toHaveBeenCalledOnce()
  })

  test('Should can not call onClick function when click and button disabled', () => {
    const fnClick = vi.fn()
    const wrapper = render(
      <Button disabled onClick={fnClick} text={buttonProps.text} className={buttonProps.className} />
    )
    expect(wrapper).toBeTruthy()

    wrapper.container.querySelector('button')?.click()
    expect(fnClick).not.toBeCalled()
  })
})
