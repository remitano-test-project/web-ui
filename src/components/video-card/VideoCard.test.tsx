import { describe, test, expect, vi } from 'vitest'
import { render, screen } from '@testing-library/react'
import VideoCard from '~/components/video-card'

describe('VideoCard component test.', () => {
  const videoCardProps = {
    videoTitle: 'Title',
    description: 'Description',
    owner: 'Owner',
    videoLink: 'https://testne.com'
  }

  afterEach(() => {
    vi.restoreAllMocks()
  })

  test('Should have correct text and iframe has correct link', () => {
    const wrapper = render(
      <VideoCard
        videoLink={videoCardProps.videoLink}
        owner={videoCardProps.owner}
        description={videoCardProps.description}
        videoTitle={videoCardProps.videoTitle}
      />
    )
    expect(wrapper).toBeTruthy()

    const iframe = wrapper.container.querySelector('iframe')
    expect(iframe?.src).toBe(videoCardProps.videoLink)

    const title = wrapper.container.querySelector('h2')
    expect(title?.textContent).toBe(videoCardProps.videoTitle)

    const owner = wrapper.container.querySelector('span')
    expect(owner?.textContent).toBe(videoCardProps.owner)

    expect(screen.getByText(videoCardProps.description)).toBeInTheDocument()
  })
})
