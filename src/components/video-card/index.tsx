import React from 'react'

interface VideoCardProps {
  videoTitle: string
  description: string
  owner: string
  videoLink: string
}

const VideoCard: React.FC<VideoCardProps> = ({ videoTitle, videoLink, description, owner }: VideoCardProps) => {
  return (
    <div className='flex max-w-3xl flex-col sm:flex-row p-3 border-solid border border-slate-200 rounded hover:shadow-lg'>
      <div className='sm:mr-6 sm:mb-0 mb-2 w-full h-full min-w-[250px]'>
        <iframe
          allow='accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share'
          className='w-full h-full'
          src={videoLink}
          title={videoTitle}
        />
      </div>
      <div className='flex flex-col text-left'>
        <h2 className='text-red-600 text-lg font-bold'>{videoTitle}</h2>
        <p className='font-bold'>
          Shared by: <span className='font-normal'>{owner}</span>
        </p>
        <p className='font-bold'>Description: </p>
        <p className='max-h-[230px] overflow-y-auto'>{description}</p>
      </div>
    </div>
  )
}

export default VideoCard
