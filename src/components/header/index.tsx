import React, { useState } from 'react'
import { Button } from '../button'
import AuthModal from '../auth-modal'
import ShareVideoModal from '../share-video-modal'
import { useAuth } from '~/providers/auth'

function Header() {
  const [isOpenAuthModal, setIsOpenAuthModal] = useState(false)
  const [isOpenShareVideoModal, setIsShareVideoModal] = useState(false)
  const [typeAuthModal, setTypeAuthModal] = useState<'Login' | 'Register'>('Login')

  const { userInfo, logout } = useAuth()

  const onLoginBtnClick = () => {
    setTypeAuthModal('Login')
    setIsOpenAuthModal(true)
  }

  const onRegisterBtnClick = () => {
    setTypeAuthModal('Register')
    setIsOpenAuthModal(true)
  }

  return (
    <div className='flex justify-between items-center mb-4'>
      <div>
        <h1 className='text-5xl font-bold'> Funny Videos</h1>
      </div>
      <div className='flex items-center'>
        {userInfo && (
          <div className='mr-2 flex items-center'>
            <p className='font-bold mr-8'>
              Welcome: <span className='font-normal'>{userInfo.email}</span>
            </p>
            <Button
              text='Share video'
              className='priority:bg-purple-700 priority:hover:bg-purple-800 mr-2'
              onClick={() => setIsShareVideoModal(true)}
            />
            <Button text='Logout' onClick={logout} />
          </div>
        )}
        {!userInfo && (
          <>
            <Button text='Login' className='mr-2' onClick={onLoginBtnClick} />
            <Button text='Register' onClick={onRegisterBtnClick} />
          </>
        )}
      </div>
      {isOpenAuthModal && <AuthModal type={typeAuthModal} handleClose={() => setIsOpenAuthModal(false)} />}
      {isOpenShareVideoModal && <ShareVideoModal handleClose={() => setIsShareVideoModal(false)} />}
    </div>
  )
}

export default Header
