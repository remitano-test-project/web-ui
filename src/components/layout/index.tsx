import React from 'react'
import Header from '../header'

const MainLayout: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  return (
    <>
      <div className='container mt-8'>
        <Header></Header>
        <div className='main'>{children}</div>
      </div>
    </>
  )
}

export default MainLayout
