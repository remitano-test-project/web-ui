import React, { ChangeEvent } from 'react'

interface InputProps {
  placeholder?: string
  onChange: (event: ChangeEvent<HTMLInputElement>) => void
  label?: string
  type?: string
  id: string
}

const Input: React.FC<InputProps> = ({ placeholder, onChange, label, id, type = 'text' }) => {
  return (
    <div className='mb-2'>
      <label htmlFor={id} className='block mb-2 text-sm font-medium text-gray-900 dark:text-white'>
        {label}
      </label>
      <input
        type={type}
        onChange={(event) => onChange(event)}
        id={id}
        className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500'
        placeholder={placeholder}
      />
    </div>
  )
}

export default Input
