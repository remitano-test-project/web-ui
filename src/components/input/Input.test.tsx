import { describe, test, expect, vi } from 'vitest'
import { fireEvent, render } from '@testing-library/react'
import Input from '~/components/input'

describe('Input component test.', () => {
  const inputProps = {
    id: 'test',
    label: 'Input Test',
    placeholder: 'Placeholder Test'
  }

  afterEach(() => {
    vi.restoreAllMocks()
  })

  test('Should show correct label and placeholder', () => {
    const fnOnChange = vi.fn()
    const wrapper = render(
      <Input id={inputProps.id} onChange={fnOnChange} label={inputProps.label} placeholder={inputProps.placeholder} />
    )
    expect(wrapper).toBeTruthy()

    const label = wrapper.container.querySelector('label')
    expect(label?.textContent).toBe(inputProps.label)

    const input = wrapper.container.querySelector('input')
    expect(input?.placeholder).toBe(inputProps.placeholder)
  })

  test('Should have correct text when typing', () => {
    const textType = 'Test huan ne'
    const fnOnChange = vi.fn()
    const wrapper = render(<Input id={inputProps.id} onChange={fnOnChange} />)
    expect(wrapper).toBeTruthy()

    const input = wrapper.container.querySelector('input')
    fireEvent.change(input as Element, { target: { value: textType } })
    expect(input?.value).toBe(textType)
  })
})
