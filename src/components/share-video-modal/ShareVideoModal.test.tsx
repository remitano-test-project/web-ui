import { describe, test, expect, vi } from 'vitest'
import { render, screen } from '@testing-library/react'
import ShareVideoModal from '~/components/share-video-modal'

describe('ShareVideoModal component test.', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  test('Should have conrrect ui', () => {
    const fnClose = vi.fn()
    const wrapper = render(<ShareVideoModal handleClose={fnClose} />)
    expect(wrapper).toBeTruthy()

    const title = screen.getByText('Share your video')
    expect(title?.tagName).toBe('P')

    const buttonLogin = screen.getByRole('button', {
      name: /Share/i
    })
    expect(buttonLogin).toBeDefined()

    const inputField = wrapper.container.querySelector('input')
    expect(inputField).toBeDefined()
  })

  test('Should have call handleClose func when close button was fire', () => {
    const fnClose = vi.fn()
    const wrapper = render(<ShareVideoModal handleClose={fnClose} />)
    expect(wrapper).toBeTruthy()

    const buttonClose = screen.getByRole('button', {
      name: /Close/i
    })
    buttonClose.click()
    expect(fnClose).toBeCalled()
  })
})
