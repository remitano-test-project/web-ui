import React, { useState } from 'react'
import ReactPortal from '../react-portal'
import Input from '../input'
import { Button } from '../button'
import { useShareVideo } from '~/hooks/video/useShareVideo'

interface ShareVideoModalProps {
  handleClose: () => void
}

const ShareVideoModal: React.FC<ShareVideoModalProps> = ({ handleClose }) => {
  const { shareYoutubeVideo } = useShareVideo()
  const [urlVideo, setUrlVideo] = useState('')

  const handleSubmit = () => {
    shareYoutubeVideo({ url: urlVideo })
    handleClose()
  }

  return (
    <ReactPortal wrapperId='share-video-modal'>
      <div className='flex justify-center items-center fixed inset-0 bg-slate-500/30'>
        <div className='w-96 p-3 bg-white rounded'>
          <p className='font-bold text-center text-lg'>Share your video</p>
          <div>
            <Input
              placeholder='Enter your video url'
              id='video_url_input'
              label='Video Url'
              onChange={(e) => {
                setUrlVideo(e.target.value)
              }}
            />
          </div>
          <div className='flex justify-center items-center mt-4'>
            <Button text='Share' className='mr-2' onClick={handleSubmit} />
            <Button className='bg-red-500 hover:bg-red-400' text='Close' onClick={handleClose} />
          </div>
        </div>
      </div>
    </ReactPortal>
  )
}

export default ShareVideoModal
