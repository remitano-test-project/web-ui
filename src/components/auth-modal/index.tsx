import React, { useState } from 'react'
import ReactPortal from '../react-portal'
import Input from '../input'
import { Button } from '../button'
import { useAuth } from '~/providers/auth'

interface AuthModalProps {
  type?: 'Login' | 'Register'
  handleClose: () => void
}

const AuthModal: React.FC<AuthModalProps> = ({ handleClose, type = 'Login' }) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const { login, register } = useAuth()

  const handleSubmit = () => {
    switch (type) {
      case 'Login':
        handleLogin()
        break
      case 'Register':
        handleRegister()
        break
      default:
        break
    }
  }

  const handleLogin = () => {
    login({
      email,
      password
    })

    handleClose()
  }

  const handleRegister = () => {
    register({
      email,
      password
    })

    handleClose()
  }

  return (
    <ReactPortal wrapperId='auth-modal'>
      <div className='flex justify-center items-center fixed inset-0 bg-slate-500/30'>
        <div className='w-96 p-3 bg-white rounded'>
          <p className='font-bold text-center text-lg'>{type}</p>
          <div>
            <Input
              placeholder='Email'
              id='email_input'
              label='Email'
              onChange={(e) => {
                setEmail(e.target.value)
              }}
            />
            <Input
              type='password'
              placeholder='Password'
              id='password_input'
              label='Password'
              onChange={(e) => {
                setPassword(e.target.value)
              }}
            />
          </div>
          <div className='flex justify-center items-center mt-4'>
            <Button text={type} className='mr-2' onClick={handleSubmit} />
            <Button className='priority:bg-red-500 priority:hover:bg-red-400' text='Close' onClick={handleClose} />
          </div>
        </div>
      </div>
    </ReactPortal>
  )
}

export default AuthModal
