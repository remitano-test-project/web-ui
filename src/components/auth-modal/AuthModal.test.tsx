import { describe, test, expect, vi } from 'vitest'
import { render, screen } from '@testing-library/react'
import AuthModal from '~/components/auth-modal'

describe('AuthModal component test.', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  test('Should have Login button when type is Login', () => {
    const fnClose = vi.fn()
    const wrapper = render(<AuthModal handleClose={fnClose} type='Login' />)
    expect(wrapper).toBeTruthy()

    const buttonLogin = screen.getByRole('button', {
      name: /Login/i
    })
    expect(buttonLogin).toBeDefined()
  })

  test('Should have Register button when type is Register', () => {
    const fnClose = vi.fn()
    const wrapper = render(<AuthModal handleClose={fnClose} type='Register' />)
    expect(wrapper).toBeTruthy()

    const buttonRegister = screen.getByRole('button', {
      name: /Register/i
    })
    expect(buttonRegister).toBeDefined()
  })

  test('Should have call handleClose func when close button was fire', () => {
    const fnClose = vi.fn()
    const wrapper = render(<AuthModal handleClose={fnClose} type='Register' />)
    expect(wrapper).toBeTruthy()

    const buttonClose = screen.getByRole('button', {
      name: /Close/i
    })
    buttonClose.click()
    expect(fnClose).toBeCalled()
  })
})
