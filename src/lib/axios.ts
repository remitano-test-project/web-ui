// eslint-disable-next-line import/no-named-as-default
import Axios, { AxiosError, InternalAxiosRequestConfig } from 'axios'

import { API_URL } from '~/config'
import storage from '~/utils/storage'

function authRequestInterceptor(config: InternalAxiosRequestConfig) {
  const token = storage.getToken()
  if (config.headers) {
    if (token) {
      config.headers.authorization = `Bearer ${token}`
    }
    config.headers.Accept = 'application/json'
  }
  return config
}

export const axios = Axios.create({
  baseURL: API_URL
})

export const coreAxios = Axios.create({
  baseURL: '/api'
})

export function isAxiosError<ResponseType>(error: unknown): error is AxiosError<ResponseType> {
  // eslint-disable-next-line import/no-named-as-default-member
  return Axios.isAxiosError(error)
}

axios.interceptors.request.use(authRequestInterceptor)
axios.interceptors.response.use((response) => {
  return response.data
})

coreAxios.interceptors.request.use(authRequestInterceptor)
coreAxios.interceptors.response.use(
  (response) => {
    return response.data
  },
  (error) => {
    return Promise.reject(error)
  }
)
