import Home from '~/pages/home'
import { AppProvider } from './providers/app'

function App() {
  return (
    <AppProvider>
      <Home />
    </AppProvider>
  )
}

export default App
