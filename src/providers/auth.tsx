import React, { useMemo, useState } from 'react'
import {
  QueryObserverResult,
  RefetchOptions,
  UseMutateAsyncFunction,
  useMutation,
  useQuery,
  useQueryClient
} from 'react-query'

import {
  LoginCredentialsDTO,
  LoginError,
  LoginResult,
  loginWithEmailAndPassword,
  RegisterCredentialsDTO,
  registerWithEmail,
  User,
  UserResponse
} from '~/features/auth'
import { getUserInfo } from '~/features/auth/api/get-userinfo'
import { isAxiosError } from '~/lib/axios'

import storage from '~/utils/storage'

export interface AuthContextValue {
  login: UseMutateAsyncFunction<
    { login_result: LoginResult } | { login_result: LoginError } | undefined,
    any,
    LoginCredentialsDTO
  >
  logout: UseMutateAsyncFunction<any, any, void, any>
  register: UseMutateAsyncFunction<any, any, RegisterCredentialsDTO>
  isLoading: boolean
  userInfo?: User
}

export interface AuthProviderProps {
  children: React.ReactNode
}

function initReactQueryAuth() {
  const AuthContext = React.createContext<AuthContextValue | null>(null)
  AuthContext.displayName = 'AuthContext'

  function AuthProvider({ children }: AuthProviderProps): JSX.Element {
    const queryClient = useQueryClient()
    const [accessToken, setAccessToken] = useState<string>(storage.getToken())

    const handleUnauthError = (error: any) => {
      if (isAxiosError(error) && (error.response?.status === 401 || error.response?.status === 403)) {
        storage.clearToken()
        // redirect to landing page
        window.location.assign(`${window.location.origin}${window.location.search}`)
      }
      return error
    }

    async function handleRegistrationResponse(data: UserResponse) {
      const { login_result } = data
      if (login_result) {
        setAccessToken(login_result.access_token)
        storage.setToken(login_result.access_token || '')
      }
      return data
    }

    function handleLogInResponse(data: LoginResult) {
      if (data.access_token) {
        setAccessToken(data.access_token)
        storage.setToken(data.access_token)
      }
      return { login_result: data }
    }

    function handleLogInErrorResponse(data: LoginError) {
      return { login_result: data }
    }

    const loginMutation = useMutation({
      mutationFn: async (data: LoginCredentialsDTO) => {
        const response = await loginWithEmailAndPassword(data)

        if (response.result) {
          return handleLogInResponse(response.result)
        }
        if (response.error) {
          return handleLogInErrorResponse(response.error)
        }
      }
    })

    const registerMutation = useMutation({
      mutationFn: async (data: RegisterCredentialsDTO) => {
        const response = await registerWithEmail(data)
        const user = await handleRegistrationResponse(response.result)

        return user
      }
    })

    const logoutMutation = useMutation({
      mutationFn: async () => {
        storage.clearToken()
        window.location.assign(window.location.origin as unknown as string)
      },
      onSuccess: () => {
        queryClient.clear()
      }
    })

    const {
      data: userInfo,
      error: userInfoError,
      isSuccess: userInfoSuccess,
      isLoading: isLoadingUserInfo,
      refetch: refetchUserInfo
    } = useQuery<any, Error>({
      queryKey: 'userInfo',
      queryFn: getUserInfo,
      enabled: !!accessToken,
      onError: handleUnauthError,
      useErrorBoundary: false
    })

    const isLoading = useMemo(
      () => loginMutation.isLoading || logoutMutation.isLoading || registerMutation.isLoading,
      [loginMutation.isLoading, logoutMutation.isLoading, registerMutation.isLoading]
    )

    const value = useMemo(
      () => ({
        login: loginMutation.mutateAsync,
        logout: logoutMutation.mutateAsync,
        register: registerMutation.mutateAsync,
        isLoading,
        userInfo: userInfo?.result
      }),
      [loginMutation.mutateAsync, logoutMutation.mutateAsync, registerMutation.mutateAsync, isLoading, userInfo]
    )

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
  }

  function useAuth() {
    const context = React.useContext(AuthContext)
    if (!context) {
      throw new Error(`useAuth must be used within an AuthProvider`)
    }
    return context
  }

  return { AuthProvider, AuthConsumer: AuthContext.Consumer, useAuth }
}

export const { AuthProvider, useAuth } = initReactQueryAuth()
