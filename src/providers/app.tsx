import * as React from 'react'
import { QueryClientProvider } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'
import { queryClient } from '~/lib/react-query'
import { AuthProvider } from './auth'
import MainLayout from '~/components/layout'

interface AppProviderProps {
  children: React.ReactNode
}

export const AppProvider = ({ children }: AppProviderProps) => {
  return (
    <QueryClientProvider client={queryClient}>
      {import.meta.env.MODE !== 'test' && <ReactQueryDevtools />}
      <AuthProvider>
        <MainLayout>{children}</MainLayout>
      </AuthProvider>
    </QueryClientProvider>
  )
}
