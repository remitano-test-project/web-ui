import React, { useState } from 'react'
import VideoCard from '~/components/video-card'
import { VideoInfo } from '~/features/video/types'
import { useGetDataVideo } from '~/hooks/video/useGetDataVideo'

const changeWatchURL2EmbedURL = (url: string) => {
  return url.replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/')
}

const Home = () => {
  const [currentPage, setCurrentPage] = useState(1)
  const { data: videoData, pagination } = useGetDataVideo(currentPage)
  return (
    <div className='mt-10 flex justify-center gap-2 flex-wrap'>
      {videoData?.map((video: VideoInfo, index: number) => (
        <VideoCard
          key={index}
          videoLink={changeWatchURL2EmbedURL(video.url)}
          videoTitle={video.title}
          description={video.description}
          owner={video.owner}
        ></VideoCard>
      ))}
    </div>
  )
}

export default Home
