import { axios } from '~/lib/axios'

import { UserResponse } from '../types'

export type RegisterCredentialsDTO = {
  email: string
  password: string
}

export interface RegisterResponse {
  result: UserResponse
}

export const registerWithEmail = async (data: RegisterCredentialsDTO): Promise<RegisterResponse> => {
  return await axios.post('/auth/register', data)
}
