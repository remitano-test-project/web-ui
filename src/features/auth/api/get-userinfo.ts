import { axios } from '~/lib/axios'

export interface UserInfo {
  email: number
}

export interface UserInfoResponse {
  result: UserInfo
}

export const getUserInfo = (): Promise<UserInfoResponse> => {
  return axios.get('/user/profile')
}
