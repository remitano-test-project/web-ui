import { axios } from '~/lib/axios'

import { LoginError, LoginResult } from '../types'

export type LoginCredentialsDTO = {
  email: string
  password: string
}

export interface LoginResponse {
  result: LoginResult
  error: LoginError
}

export const loginWithEmailAndPassword = (data: LoginCredentialsDTO): Promise<LoginResponse> => {
  return axios.post('/auth/login', data)
}
