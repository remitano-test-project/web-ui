export interface LoginResult {
  access_token: string
  expires_in: number
  refresh_token: string
  user_info: {
    email: string
  }
}

export interface LoginError {
  code: string
  message: string
}

export interface User {
  email?: string
  password?: string
}

export interface UserResponse {
  email?: string
  login_result?: LoginResult
}
