import { axios } from '~/lib/axios'
import { QuerySearch, VideoResponse, ShareVideoRequest } from '../types'

export const shareVideo = async (data: ShareVideoRequest): Promise<VideoResponse> => {
  return await axios.post('/video/share', data)
}

export const getVideos = async (params: QuerySearch): Promise<VideoResponse> => {
  return await axios.get('/video', {
    params
  })
}
