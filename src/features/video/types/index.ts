export interface VideoInfo {
  id?: string
  url: string
  title: string
  description: string
  owner: string
}

export interface Pagination {
  page: number
  total: number
}

export interface VideoResponse {
  data: VideoInfo[]
  pagination: Pagination
}

export type QuerySearch = {
  page: number
}

export type ShareVideoRequest = {
  url: string
}
